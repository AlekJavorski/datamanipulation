import unittest
import program
import pandas as pd


class TestDoTask(unittest.TestCase):

    test_df = pd.DataFrame({'Index': [z for z in range(100)], 'A': [(x-50)**2 for x in range(100)], 'B' : [-(x-50)**2 for x in range(100)]})

    def test(self):
        self.assertEqual(program.extract_minimum_difference(self.test_df, ('A', 'B')), 50)
