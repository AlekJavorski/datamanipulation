import io
import pandas as pd
import requests


def get_data_from_web(url):
    """
    downloads the data to be used to construct the pandas dataframe
    :param url: the url to be requested
    :return: object downloaded as iostring
    """
    out = io.StringIO()
    data = requests.get(url, stream=True)
    for line in data.iter_lines():
        replaced = line.decode('UTF-8').replace('-', '').replace('*', '') #cleaning the data of asterisks and dashes.
        out.write(f'{replaced}\n')
    out.seek(0)
    return out


def get_dataframe_from_web(url):
    """
    creates a pandas dataframe
    :param url: the url to be requested
    :return: a pandas dataframe made from the resource requested by the url
    """
    with get_data_from_web(url) as data:
        return pd.read_table(data, delim_whitespace=True)


def extract_minimum_difference(df, indices):
    """
    :param df: a pandas dataframe to be used
    :param indices: indices of two columns to be compared as a tuple
    :return: the value of the first column for the entry with the lowest abs. difference between two columns specified
    """
    first_index, second_index = indices
    df_reindexed = df.reset_index(drop=True)
    spreads = abs(df_reindexed[first_index] - df_reindexed[second_index])
    min_id = int(spreads.idxmin())
    return df_reindexed.iat[min_id, 0]


if __name__ == '__main__':
    import logging as log
    log.basicConfig(filename='logs.log')

    weather_url = 'http://codekata.com/data/04/weather.dat'
    football_url = 'http://codekata.com/data/04/football.dat'
    try:
        weather_result = extract_minimum_difference(get_dataframe_from_web(weather_url), ("MnT", "MxT"))
        print(f'The day with the lowest temperature spread is the day number: {weather_result}')

        football_result = extract_minimum_difference(get_dataframe_from_web(football_url), ('A', 'F'))
        print(f'The team with the lowest for and against goal difference is: {football_result}')
    except Exception as e:
        log.exception(e)

